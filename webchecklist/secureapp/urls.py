from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path("", views.home, name=""),
    path("register", views.register, name="register"),
    path("dashboard", views.dashboard, name="dashboard"),
    path("user-logout", views.user_logout, name="user-logout"),
    path("account-locked", views.account_locked, name="account-locked"),
    # Password management
    path(
        "reset-password",
        auth_views.PasswordResetView.as_view(
            template_name="password/password-reset.html"
        ),
        name="reset-password",
    ),
    path(
        "reset-password-sent",
        auth_views.PasswordResetDoneView.as_view(
            template_name="password/password-sent.html"
        ),
        name="reset-password-sent",
    ),
    path(
        "reset/<uidb64>/token/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="password/password-reset-form.html"
        ),
        name="password-reset-confirm",
    ),
    path(
        "password-reset-complete",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="password/password-reset-complete.html"
        ),
        name="password-reset-complete",
    ),
]
